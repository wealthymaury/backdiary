from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from rest_framework import routers

from app.views import LoginFormView, AppTemplateView
from app.viewsets import UserViewSet, UserProfileViewSet, PhoneViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'userprofiles', UserProfileViewSet)
router.register(r'phones', PhoneViewSet)

urlpatterns = [
	url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', LoginFormView.as_view(), name="home"),
    url(r'^contactos/', AppTemplateView.as_view(), name="app"),
    url(r'^api/', include(router.urls)), # api URLs
    url(r'^api-auth/', include('rest_framework.urls'), name='rest_framework'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'})
]

if settings.DEBUG:
	urlpatterns += [
		url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
	]