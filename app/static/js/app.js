$.ajaxSetup(
{
    beforeSend: function(xhr, settings)
    {
    	function csrfSafeMethod(method)
		{
		    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
		}
        if(!csrfSafeMethod(settings.type) && !this.crossDomain)
        {
            xhr.setRequestHeader("X-CSRFToken", token);
        }
    }
});

var Backdiary = new Marionette.Application();

Backdiary.addRegions(
{
	headerRegion: '#header-region',
	mainRegion: '#main-region',
	dialogRegion: Marionette.Region.Dialog.extend({
		el: '#dialog-region'
	})
});

Backdiary.navigate = function(route, options)
{
	options || (options = {})
	Backbone.history.navigate(route);
}
Backdiary.getCurrentRoute = function()
{
	return Backbone.history.fragment;
}

Backdiary.on('start', function()
{	
	if(Backbone.history)
	{
		Backbone.history.start();
		
		if(this.getCurrentRoute() === "")
		{
			Backdiary.trigger('contacts:list')
		}
	}

	this.Socket = new Socket(url_socket);
	this.Socket.on('message', function(message)
	{
		var msg = 'Un ' + models[message.model_name] + ' ha sido ' + acciones[message.action];
		Backdiary.trigger('notify:success', msg);
	});

	Notification.requestPermission();
});
