Backdiary.module('Entities', function(Entities, Backdiary, Backbone, Marionette, $, _)
{
	var BaseModel = Backbone.Model.extend(
	{	
		rollback : function()
		{
			this.set(this.previousAttributes());
		}
	});

	Entities.ContactModel = BaseModel.extend(
	{
		validation: true,
		urlRoot: '/api/users/',
		defaults: {
			first_name: '',
			last_name: '',
			username: '',
			email: '',
		},
		validatorRules: {
			first_name: 'required|string',
			last_name: 'required|string',
			username: 'required|string',
			email: 'required|string|email',
		}
	});

	Entities.UserprofileModel = BaseModel.extend(
	{
		validation: true,
		urlRoot: '/api/userprofiles/',
		defaults: {
			user: '',
			job: '',
			avatar: '',
			timestamp: '',
		},
		validatorRules: {
			job: 'required|string',
			avatar: 'required|mimes:png,jpg,jpeg',
			timestamp: '',
		}
	});

	Entities.PhoneModel = Backbone.Model.extend(
	{
		validation: true,
		urlRoot: '/api/phones/',
		defaults: {
			number: '',
			type: '',
			user: '',
			timestamp: ''
		},
		validatorRules: {
			number: 'required|string|length:10',
			type: 'required|in:cel,nex,tel',
			user: 'required',
			timestamp: ''
		},
		rollback : function()
		{
			this.set(this.previousAttributes());
		},
	});

	Entities.ContactsCollection = Backbone.Collection.extend(
	{
		url: '/api/users/',
		model: Entities.ContactModel
	});

	Entities.UserprofilesCollection = Backbone.Collection.extend(
	{
		url: '/api/users/',
		model: Entities.UserprofileModel
	});

	Entities.PhonesCollection = Backbone.Collection.extend(
	{
		urlRoot: '/api/userprofiles/',
		user: undefined,
		url: function()
		{	
			return this.urlRoot + this.user + '/phones';
		},
		initialize: function(options)
		{
			this.user = options.user;
		},
		model: Entities.PhoneModel
	});

	var API = {
		options: function(defer){
			return {
				success: function(data)
				{	
					defer.resolve(data);
				},
				error: function(data)
				{
					defer.resolve(undefined);
				}
			}
		},
		getContactEntities: function()
		{
			var contacts = new Entities.ContactsCollection();
			var defer = $.Deferred();
			var options = API.options(defer);
			
			contacts.fetch(options);
			
			return defer.promise();
		},
		getContactEntity: function(contactId)
		{	
			var contact = new Entities.ContactModel({id: contactId});
			var defer = $.Deferred();
			var options = API.options(defer);

			contact.fetch(options);
			
			return defer.promise();
		},
		getUserprofiles: function()
		{
			var userprofiles = new Entities.UserprofilesCollection();
			var defer = $.Deferred();
			var options = API.options(defer);

			userprofiles.fetch(options);
			
			return defer.promise();
		},
		getUserprofile: function(id)
		{
			var userprofile = new Entities.UserprofileModel({id: id});
			var defer = $.Deferred();
			var options = API.options(defer);

			userprofile.fetch(options);
			
			return defer.promise();
		},
		getPhones: function(user)
		{	
			var phones = new Entities.PhonesCollection({user: user});
			var defer = $.Deferred();
			var options = API.options(defer);

			phones.fetch(options);
			
			return defer.promise();
		},
		getPhone: function(id)
		{
			var phone = new Entities.PhoneModel({id: id});
			var defer = $.Deferred();
			var options = API.options(defer);

			phone.fetch(options);
			
			return defer.promise();
		},
	};

	Backdiary.reqres.setHandler('contact:entities', function()
	{
		return API.getContactEntities();
	});

	Backdiary.reqres.setHandler('contact:entity', function(id)
	{	
		return API.getContactEntity(id);
	});

	Backdiary.reqres.setHandler('userprofile:entities', function()
	{
		return API.getUserprofiles();
	});

	Backdiary.reqres.setHandler('userprofile:entity', function(id)
	{	
		return API.getUserprofile(id);
	});

	Backdiary.reqres.setHandler('phone:entities', function(user)
	{
		return API.getPhones(user);
	});

	Backdiary.reqres.setHandler('phone:entity', function(id)
	{	
		return API.getPhone(id);
	});

});