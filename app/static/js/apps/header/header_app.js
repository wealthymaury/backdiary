Backdiary.module('HeaderApp', function(Header, Backdiary, Backbone, Marionette, $, _)
{
	var API = {
		listHeader: function()
		{
			Header.List.Controller.listHeader();
		}
	}

	Header.on('start', function()
	{	
		API.listHeader();
	});
});