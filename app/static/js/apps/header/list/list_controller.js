Backdiary.module('HeaderApp.List', function(List, Backdiary, Backbone, Marionette, $, _)
{
	List.Controller = {
		listHeader: function()
		{	
			var header = new List.Header();
			header.on('contact:close', function()
			{
				Backdiary.trigger('session:logout');
			});
			header.on('contact:add', function()
			{
				Backdiary.trigger('contact:new');
			});
			header.on('contact:list', function()
			{
				Backdiary.trigger('contacts:list');
			});
			Backdiary.headerRegion.show(header);
		}
	}
});