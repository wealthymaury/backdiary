Backdiary.module('HeaderApp.List', function(List, Backdiary, Backbone, Marionette, $, _)
{
	List.Header = Marionette.ItemView.extend(
	{
		template: '#header-template',
		tagName: 'nav',
		className: 'teal nav-bottom',
		triggers: {
			'click a.js-list': 'contact:list',
			'click a.js-close': 'contact:close',
			'click a.js-new': 'contact:add'
		}
	});
});