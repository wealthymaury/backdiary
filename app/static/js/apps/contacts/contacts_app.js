Backdiary.module('ContactsApp', function(ContactsApp, Backdiary, Backbone, Marionette, $, _)
{
	ContactsApp.Router = Marionette.AppRouter.extend(
	{
		appRoutes: {
			'contacts': 'listContacts',
			'contacts/add': 'newContact',
			'contacts/:id': 'showContact',
			'contacts/:id/edit': 'editContact',
			'contacts/:id/phone/add': 'addPhoneToContact',
		}
	});

	var API = {
		listContacts: function()
		{
			ContactsApp.List.Controller.listContacts();
		},
		newContact: function()
		{	
			ContactsApp.New.Controller.newContact();
		},
		showContact: function(id)
		{	
			ContactsApp.Show.Controller.showContact(id);
		},
		editContact: function(id)
		{
			ContactsApp.Edit.Controller.editContact(id);
		},
		addPhoneToContact: function(user)
		{
			ContactsApp.New.Controller.newPhone(user);
		}
	};

	Backdiary.on('contacts:list', function()
	{
		Backdiary.navigate("contacts");
		API.listContacts();
	});

	Backdiary.on('contact:new', function()
	{
		Backdiary.navigate("contacts/add");
		API.newContact();
	});

	Backdiary.on('contact:show', function(id)
	{
		Backdiary.navigate("contacts/" + id);
		API.showContact(id);
	});

	Backdiary.on('contact:edit', function(id)
	{
		Backdiary.navigate("contacts/" + id + "/edit");
		API.editContact(id);
	});

	Backdiary.on('contact:new:phone', function(user)
	{
		Backdiary.navigate("contacts/" + user + "/phone/add");
		API.addPhoneToContact(user);
	});

	Backdiary.addInitializer(function(){
		new ContactsApp.Router({
			controller: API
		});
	});
});