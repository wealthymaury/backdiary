Backdiary.module('ContactsApp.New', function(New, Backdiary, Backbone, Marionette, $, _)
{
	New.Contact = Backdiary.Common.Views.Form.extend(
	{
		title: 'Nuevo usuario',
		onRender: function()
		{
			this.$('.js-title').text(this.title);
			this.$('.js-submit').text('Crear contacto');
		}
	});

	New.Phone = Backdiary.Common.Views.Form.extend(
	{
		template: '#phone-form-template',
		title: 'Nuevo telefono',
		onRender: function()
		{
			this.$('.js-title').text(this.title);
			this.$('.js-submit').text(this.title.toUpperCase());
		},
		onShow: function(view)
		{
			var activeLabels = function()
			{
				var $groups = view.$('.input-field');
				$.each($groups, function(index, element)
				{
					if($(element).find('input').val() !== '')
					{
						$(element).find('label').addClass('active');
					}
				});
			}
			this.setTitleModal(view, this.title);
			activeLabels();
		},
		setTitleModal: function(view, title)
		{	
			view._parent.$el.find('.js-title').html(title);
		},
	});
});