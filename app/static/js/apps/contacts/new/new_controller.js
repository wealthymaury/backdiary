Backdiary.module('ContactsApp.New', function(New, Backdiary, Backbone, Marionette, $, _)
{
	New.Controller = {
		newContact: function()
		{	
			var newContact = new Backdiary.Entities.ContactModel();
			var view = new Backdiary.ContactsApp.New.Contact(
			{
				model: newContact
			});

			view.on('form:submit', function(data)
			{	
				options = {
					success: function(model, resp, req)
					{	
						Backdiary.trigger('contacts:list');
						Backdiary.trigger('notify:success', 'Operación exitosa.');
					},
					error: function(model, resp, req)
					{	
						newContact.rollback();
						view.triggerMethod('form:data:invalid', resp.responseJSON);
						Backdiary.trigger('notify:error', 'Error al guardar los datos');
					},
					wait: true
				}

				newContact.set(data);

				if( ! newContact.save(data, options))
				{
					newContact.rollback();
					view.triggerMethod('form:data:invalid', newContact.validationError);
					Backdiary.trigger('notify:error', 'Error de validación');
				}

			});
			Backdiary.mainRegion.show(view);
		},
		newPhone: function(user)
		{
			var newPhone = new Backdiary.Entities.PhoneModel({user: user});
			var view = new Backdiary.ContactsApp.New.Phone(
			{
				model: newPhone
			});

			view.on('form:submit', function(data)
			{	
				options = {
					success: function(model, resp, req)
					{	
						Backdiary.trigger('contact:show', user);
						Backdiary.trigger('notify:success', 'Operación exitosa.');
					},
					error: function(model, resp, req)
					{	
						newPhone.rollback();
						view.triggerMethod('form:data:invalid', resp.responseJSON);
						Backdiary.trigger('notify:error', 'Error al guardar los datos');
					},
					wait: true
				}

				newPhone.set(data);

				if( ! newPhone.save(data, options))
				{
					newPhone.rollback();
					view.triggerMethod('form:data:invalid', newPhone.validationError);
					Backdiary.trigger('notify:error', 'Error de validación');
				}

			});
			Backdiary.mainRegion.show(view);
		}
	}
});