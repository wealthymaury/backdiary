Backdiary.module('ContactsApp.Show', function(Show, Backdiary, Backbone, Marionette, $, _)
{
	Show.Controller = {
		showContact: function(id)
		{	
			var loadingView = new Backdiary.Common.Views.Loading();
			Backdiary.mainRegion.show(loadingView);

			var fetchingContact = Backdiary.request('userprofile:entity', id);
			fetchingContact.done(function(model)
			{
				var layout = new Show.Layout();
				
				var profile = new Show.Profile(
				{
					model: model
				});
				
				var account = new Show.Account(
				{
					model: new Backdiary.Entities.ContactModel(model.toJSON().user)
				});

				profile.on('profile:edit', function(model)
				{
					var modal = new Backdiary.Common.Views.Modal();

					var view = new Backdiary.ContactsApp.Edit.Profile(
					{
						model: model
					});

					modal.on('show', function()
					{
						modal.contentRegion.show(view);
					});

					view.on('form:submit', function(data)
					{	
						options = {
							success: function(model, resp, req)
							{	debugger;
								Backdiary.trigger('notify:success', 'Operación exitosa.');
								modal.triggerMethod('dialog:close');
								profile.render();
							},
							error: function(model, resp, req)
							{	debugger;
								model.rollback();
								view.triggerMethod('form:data:invalid', resp.responseJSON);
								Backdiary.trigger('notify:error', 'Error al guardar los datos');
							},
							//wait: true
						}

						model.set(data);
						
						if( ! model.save(data, options) )
						{
							model.rollback();
							view.triggerMethod('form:data:invalid', model.validationError);
							Backdiary.trigger('notify:error', 'Error de validación');
						}else
						{
							modal.triggerMethod('dialog:close');
						}
					});

					Backdiary.dialogRegion.show(modal);
				});

				account.on('contact:edit', function(model)
				{	
					//esto es para editar en vista independiente
					Backdiary.trigger('contact:edit', model.get('id'));

					//esto es para editar con modal y si funciona
					// var modal = new Backdiary.Common.Views.Modal();

					// var form = new Backdiary.ContactsApp.Edit.Contact(
					// {
					// 	model: model
					// });

					// modal.on('show', function()
					// {
					// 	modal.contentRegion.show(form);
					// });

					// form.on('form:submit', function(data)
					// {	
					// 	options = {
					// 		success: function(model, resp, req)
					// 		{	
					// 			Backdiary.trigger('notify:success', 'Operación exitosa.');
					// 		},
					// 		error: function(model, resp, req)
					// 		{	
					// 			model.rollback();
					// 			form.triggerMethod('form:data:invalid', resp.responseJSON);
					// 			Backdiary.trigger('notify:error', 'Error al guardar los datos');
					// 		},
					// 		wait: true
					// 	}

					// 	model.set(data);

					// 	if(model.save(data, options))
					// 	{
					// 		account.render();
					// 		modal.triggerMethod('dialog:close');
					// 	}
					// 	else
					// 	{
					// 		model.rollback();
					// 		form.triggerMethod('form:data:invalid', model.validationError);
					// 		Backdiary.trigger('notify:error', 'Error de validación');
					// 	}

					// });
					// Backdiary.dialogRegion.show(modal);
				});
				
				var fetchingPhonesCollection = Backdiary.request('phone:entities', model.get('id'));
				fetchingPhonesCollection.done(function(data)
				{
					var phones = new Show.Phones(
					{
						collection: data
					});
					phones.user = model.get('id');

					phones.on('new:phone', function(user)
					{
						Backdiary.trigger('contact:new:phone', user);
					});

					phones.on('childview:delete:phone', function(childview, model)
					{	
						model.destroy({
							wait: true,
							success: function(){
								Backdiary.trigger('notify:success', 'Operación exitosa.');
							},
							error: function()
							{
								Backdiary.trigger('notify:error', 'No se pudo completar la eliminación.');
							}
						});
					});

					phones.on('childview:edit:phone', function(childview, model)
					{	
						var modal = new Backdiary.Common.Views.Modal();

						var view = new Backdiary.ContactsApp.New.Phone(
						{
							model: model
						});
						view.title = 'Editar telefono';

						modal.on('show', function()
						{
							modal.contentRegion.show(view);
						});

						view.on('form:submit', function(data)
						{	
							options = {
								success: function(model, resp, req)
								{	
									Backdiary.trigger('notify:success', 'Operación exitosa.');
								},
								error: function(model, resp, req)
								{	
									model.rollback();
									view.triggerMethod('form:data:invalid', resp.responseJSON);
									Backdiary.trigger('notify:error', 'Error al guardar los datos');
								},
								wait: true
							}

							model.set(data);

							if(model.save(data, options))
							{
								childview.render();
								modal.triggerMethod('dialog:close');
							}
							else
							{
								model.rollback();
								view.triggerMethod('form:data:invalid', model.validationError);
								Backdiary.trigger('notify:error', 'Error de validación');
							}

						});

						Backdiary.dialogRegion.show(modal);						
					});

					layout.on('show', function()
					{
						layout.profileRegion.show(profile);
						layout.accountRegion.show(account);
						layout.phonesRegion.show(phones);
					});
					
					Backdiary.mainRegion.show(layout);

					//real-time
					Backdiary.Socket.on('phone:create', function(id, data, message)
					{	
						if(this.user == data.data.user)
						{
							this.add(data.data);
						}
					}, data);

					Backdiary.Socket.on('phone:update', function(id, data, message)
					{	
						var model = this.find({id: +id});
						model.set(data.data);
					}, data);

					Backdiary.Socket.on('phone:delete', function(id, data, message)
					{	
						this.remove(id);
					}, data);
				});

				//el create no me interesa aqui

				Backdiary.Socket.on('userprofile:update', function(id, data, message)
				{	
					if(this.get('id') == data.data.id)
					{
						this.set(data.data);
					}
				}, model);

				Backdiary.Socket.on('userprofile:delete', function(id, data, message)
				{	
					if(this.get('id') == id)
					{
						Backdiary.trigger('contacts:list');
					}
				}, model);

			});
		}
	};
});