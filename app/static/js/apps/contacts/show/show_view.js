Backdiary.module('ContactsApp.Show', function(Show, Backdiary, Backbone, Marionette, $, _)
{
	Show.Layout = Marionette.LayoutView.extend(
	{
		template: '#contact-detail-layout-template',
		className: 'paddingable',
		regions: {
			profileRegion: '#profile-region',
			accountRegion: '#account-region',
			phonesRegion: '#phones-region'
		}
	});

	Show.Profile = Marionette.ItemView.extend(
	{
		template: '#contact-profile-template',
		tagName: 'div',
		className: 'row',
		events: {
			'click .js-edit': 'editClicked'
		},
		modelEvents: {
			'change': 'render'
		},
		editClicked: function(e)
		{
			e.preventDefault();
			this.trigger('profile:edit', this.model);
		}
	});

	Show.Account = Marionette.ItemView.extend(
	{
		template: '#contact-account-template',
		tagName: 'div',
		events: {
			'click .js-edit': 'editClicked'
		},
		editClicked: function(e)
		{
			e.preventDefault();
			this.trigger('contact:edit', this.model);
		}
	});

	var NoPhonesView = Marionette.ItemView.extend(
	{
		template: '#contact-phone-none'
	});

	Show.Phone = Marionette.ItemView.extend(
	{
		template: '#contact-phone-template',
		tagName: 'div',
		className: 'Chip',
		events: {
			'click .js-delete': 'deleteClicked',
			'click .js-edit': 'editClicked'
		},
		modelEvents: {
			'change': 'render'
		},
		deleteClicked: function(e)
		{
			e.preventDefault();
			this.trigger('delete:phone', this.model);
		},
		editClicked: function(e)
		{
			e.preventDefault();
			this.trigger('edit:phone', this.model);
		}
	});

	Show.Phones = Marionette.CompositeView.extend(
	{
		template: '#contact-phones-template',
		emptyView: NoPhonesView,
		childView: Show.Phone,
		childViewContainer: '.phones',
		events: {
			'click .js-add': 'addClicked'
		},
		addClicked: function(e)
		{
			e.preventDefault();
			this.trigger('new:phone', this.user);
		}
	});
});