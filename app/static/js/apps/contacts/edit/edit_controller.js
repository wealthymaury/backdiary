Backdiary.module('ContactsApp.Edit', function(Edit, Backdiary, Backbone, Marionette, $, _)
{
	Edit.Controller = {
		editContact: function(id)
		{
			var loadingView = new Backdiary.Common.Views.Loading();
			Backdiary.mainRegion.show(loadingView);

			var fetchingContact = Backdiary.request('contact:entity', id);
			$.when(fetchingContact).done(function(contact)
			{
				var form = new Backdiary.ContactsApp.Edit.Contact(
				{
					model: contact
				});

				form.on('form:submit', function(data)
				{	
					options = {
						success: function(model, resp, req)
						{	
							Backdiary.trigger('contacts:list');
							Backdiary.trigger('notify:success', 'Operación exitosa.');
						},
						error: function(model, resp, req)
						{	
							model.rollback();
							form.triggerMethod('form:data:invalid', resp.responseJSON);
							Backdiary.trigger('notify:error', 'Error al guardar los datos');
						},
						wait: true
					}

					contact.set(data);

					if( ! contact.save(data, options) )
					{
						contact.rollback();
						form.triggerMethod('form:data:invalid', contact.validationError);
						Backdiary.trigger('notify:error', 'Error de validación');
					}

				});
				Backdiary.mainRegion.show(form);
			});
		}
	}
});