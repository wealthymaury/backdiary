Backdiary.module('ContactsApp.Edit', function(Edit, Backdiary, Backbone, Marionette, $, _)
{
	Edit.Contact = Backdiary.Common.Views.Form.extend(
	{
		template: '#contact-form',
		tagName: 'form',
		className: 'section paddingable',
		onShow: function(view)
		{
			var activeLabels = function()
			{
				var $groups = view.$('.input-field');
				$.each($groups, function(index, element)
				{
					if($(element).find('input').val() !== '')
					{
						$(element).find('label').addClass('active');
					}
				});
			}
			this.setTitleModal(view, 'Editar contacto');
			activeLabels();
		},
		setTitleModal: function(view, title)
		{	
			view._parent.$el.find('.js-title').html(title);
		}
	});

	Edit.Profile = Backdiary.Common.Views.Form.extend(
	{
		template: '#profile-form',
		tagName: 'form',
		className: 'section paddingable',
		events:{
			'change #avatar-input' : 'loadImage',
			'click button.js-submit': 'submitClicked'
		},
		onShow: function(view)
		{
			var activeLabels = function()
			{
				var $groups = view.$('.input-field');
				$.each($groups, function(index, element)
				{
					if($(element).find('input').val() !== '')
					{
						$(element).find('label').addClass('active');
					}
				});
			}
			this.setTitleModal(view, 'Editar contacto');
			activeLabels();
		},
		setTitleModal: function(view, title)
		{	
			view._parent.$el.find('.js-title').html(title);
		},
		loadImage: function()
		{
			var p = new pic64();

			p.context = this;
			p.onready = function(e)
			{
				this.context.setImage(this.getImageBase64());
			}

			p.readImageFrom('avatar-input');
		},
		setImage: function(img)
		{
			this.$('#preview-avatar').attr('src', img);
			this.$('#contact-avatar').val(img);
		}
	});
});