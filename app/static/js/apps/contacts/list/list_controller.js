Backdiary.module('ContactsApp.List', function(List, Backdiary, Backbone, Marionette, $, _)
{
	List.Controller = {
		listContacts: function()
		{
			var loadingView = new Backdiary.Common.Views.Loading();
			Backdiary.mainRegion.show(loadingView);
			
			var fetchingContacts = Backdiary.request('contact:entities');
			$.when(fetchingContacts).done(function(contacts)
			{	
				var contactListView = new List.Contacts({
					collection: contacts
				});

				contactListView.on('childview:contact:show', function(childView, model)
				{
					Backdiary.trigger('contact:show', model.get('id'));
				});

				contactListView.on('childview:contact:delete', function(childView, model)
				{
					model.destroy({
						wait: true,
						success: function(){
							Backdiary.trigger('notify:success', 'Operación exitosa.');
						},
						error: function()
						{
							Backdiary.trigger('notify:error', 'No se pudo completar la eliminación.');
						}
					});
				});

				contactListView.on('childview:contact:edit', function(childView, model)
				{
					var modal = new Backdiary.Common.Views.Modal();

					var form = new Backdiary.ContactsApp.Edit.Contact(
					{
						model: model
					});

					modal.on('show', function()
					{
						modal.contentRegion.show(form);
					});

					form.on('form:submit', function(data)
					{	
						options = {
							success: function(model, resp, req)
							{	
								Backdiary.trigger('notify:success', 'Operación exitosa.');
							},
							error: function(model, resp, req)
							{	
								model.rollback();
								form.triggerMethod('form:data:invalid', resp.responseJSON);
								Backdiary.trigger('notify:error', 'Error al guardar los datos');
							},
							wait: true
						}

						model.set(data);

						if(model.save(data, options))
						{
							childView.render();
							modal.triggerMethod('dialog:close');
						}
						else
						{
							model.rollback();
							form.triggerMethod('form:data:invalid', model.validationError);
							Backdiary.trigger('notify:error', 'Error de validación');
						}

					});
					Backdiary.dialogRegion.show(modal);
				});

				Backdiary.mainRegion.show(contactListView);
				
				Backdiary.Socket.on('user:create', function(id, data, message)
				{	
					this.add(data.data);
				}, contacts);

				Backdiary.Socket.on('user:update', function(id, data, message)
				{
					var model = this.find({id: +id});
					model.set(data.data);
				}, contacts);

				Backdiary.Socket.on('user:delete', function(id, data, message)
				{
					this.remove(id);
				}, contacts);

			});
		}
	}
});