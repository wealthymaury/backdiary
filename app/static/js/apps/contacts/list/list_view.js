Backdiary.module('ContactsApp.List', function(List, Backdiary, Backbone, Marionette, $, _)
{

	List.Contact = Marionette.ItemView.extend(
	{
		tagName: 'li',
		template: '#contact-item-template',
		className: 'collection-item avatar fix-padding-left',
		events: {
			'click i.js-delete' : 'deleteClicked',
			'click i.js-show' : 'showClicked',
			'click i.js-edit' : 'editClicked'
		},
		modelEvents : {
			'change' : 'render'
		},
		flash: function(cssClass)
		{
			var $view = this.$el;
			$view.hide().fadeIn(200, function() 
			{
				$view.fadeOut(200).fadeIn(500);
			});;
		},
		editClicked: function(e)
		{	
			e.preventDefault();
			e.stopPropagation();
			this.trigger('contact:edit', this.model);
		},
		showClicked: function(e)
		{	
			e.preventDefault();
			e.stopPropagation();
			this.trigger('contact:show', this.model);
		},
		deleteClicked: function(e)
		{
			e.stopPropagation();
			this.trigger('contact:delete', this.model);
		},
		remove: function()
		{
			var self = this;
			this.$el.fadeOut('slow', function()
			{
				Marionette.ItemView.prototype.remove.call(self);
			});
		}
	});

	var NoContactsView = Marionette.ItemView.extend(
	{
		template: '#contact-none-template',
		tagName: 'li',
		className: 'collection-item avatar fix-padding-left'
	});

	List.Contacts = Marionette.CompositeView.extend(
	{
		tagName: 'ul',
		className: 'collection with-header border-none paddingable',
		template: '#contacts-list-template',
		emptyView: NoContactsView,
		childView: List.Contact,
		childViewContainer: '.content',
		attachHtml: function(collectionView, childView, index){
			if (collectionView.isBuffering)
			{
				collectionView._bufferedChildren.splice(index, 0, childView);
			}
			else
			{
				if (!collectionView._insertBefore(childView, -1))
				{
					collectionView._insertAfter(childView);
				}
				childView.flash('grey lighten-4');
			}
		}
	});

});