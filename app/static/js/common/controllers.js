Backdiary.module('Common.Controllers', function(Controllers, Backdiary, Backbone, Marionette, $, _)
{
	Controllers.Session = {
		logout: function()
		{
			console.log('logout');
			window.location = '/logout/';
		}
	}

	Controllers.Notify = {
		success: function(msg)
		{
			console.log(msg);
			Materialize.toast(msg, 4000);
			var n = new Notification('New notification', {'body': msg});
			$('#audio_success')[0].play();
		},
		error: function(msg)
		{
			console.log(msg);
			$('#audio_error')[0].play();
			Materialize.toast(msg, 4000);
		}
	}

	Backdiary.on('session:logout', function()
	{
		Controllers.Session.logout();
	});

	Backdiary.on('notify:error', function(msg)
	{
		Controllers.Notify.error(msg);
	});

	Backdiary.on('notify:success', function(msg)
	{
		Controllers.Notify.success(msg);
	});

});