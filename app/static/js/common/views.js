Backdiary.module('Common.Views', function(Views, Backdiary, Backbone, Marionette, $, _)
{
	Views.Modal = Marionette.LayoutView.extend(
	{
		template: '#modal-template',
		regions: {
			contentRegion: '#modal-content-region'
		},
		onDialogClose: function()
		{
			this.$el.closeModal();
		},
	});

	Views.Loading = Marionette.ItemView.extend(
	{
		template: '#loading-view',
		className: 'center-align',
		initialize: function(options)
		{
			var options = options || {};
			this.title = options.title || 'Loading data...';
		},
		serializeData: function()
		{
			return {
				title: this.title
			}
		}
	});

	Views.Form = Marionette.ItemView.extend(
	{
		template: '#contact-form',
		tagName: 'form',
		className: 'section paddingable',
		events: {
			'click button.js-submit': 'submitClicked'
		},
		submitClicked: function(e)
		{
			e.preventDefault();
			//esto es importante, usando syphon puedo extraer la data del formulario
			var data = Backbone.Syphon.serialize(this);
			this.trigger('form:submit', data);
		},
		//este evento lo dispara el controller, linea 31
		onFormDataInvalid: function(errors)
		{	
			var $form = this.$el;
			var clearFormErrors = function()
			{
				$form.find('.error-helper').each(function()
				{	
					$(this).remove();
				});
			}
			var markErrors = function(value, key)
			{	
				var $formGroup = $form.find('#contact-' + key).parent();
				var $errorEl = $('<span>', {class: 'error-helper', text: value[0] });
				$formGroup.append($errorEl);
				$formGroup.find('input').addClass('invalid');
			}

			clearFormErrors();
			_.each(errors, markErrors);
		},
	});
});