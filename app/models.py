from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator
from django.utils.timesince import timesince

from sorl.thumbnail import get_thumbnail

class Phone(models.Model):
	PHONES_TYPES = (
		('cel', 'Celular'),
		('tel', 'Fijo'),
		('nex', 'Nextel'),
	)

	number = models.CharField(max_length=10, unique=True, validators=[ MinLengthValidator(10) ])
	type = models.CharField(max_length=3, choices=PHONES_TYPES)
	timestamp = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User)

	def __str__(self):
		return "%s - %s" % (self.number, self.type)

class UserProfile(models.Model):
	job = models.CharField(max_length=60)
	avatar = models.ImageField(upload_to='users', default='users/user.jpg')
	timestamp = models.DateTimeField(auto_now_add=True)
	user = models.OneToOneField(User, unique=True)
	phones = models.ManyToManyField(Phone)

	def date(self):
		return timesince(self.timestamp)

	def get_thumb_avatar(self):
		if self.avatar:
			return get_thumbnail(self.avatar, '30x30', crop='center', format='PNG').url

	def __str__(self):
		return "%s - %s" % (self.user, self.job)

# permite que al crear un usuario, automaticamente se genere su perfil
# se pone algo tambien en el settings
def create_user_profile(sender, instance, created, **kwargs):
	if created:
		UserProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)