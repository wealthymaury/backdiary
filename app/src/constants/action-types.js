export const ADD_USER = "ADD_USER";
export const ADD_USER_ERROR = "ADD_USER_ERROR";
export const EDIT_USER = "EDIT_USER";
export const ADD_PHONE = "ADD_PHONE";
export const EDIT_PHONE = "EDIT_PHONE";
export const FOUND_BAD_WORD = "FOUND_BAD_WORD";
export const USERS_LOADED = "USERS_LOADED";
