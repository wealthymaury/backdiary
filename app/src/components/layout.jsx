import React, { Component } from 'react';

import Header from './header.jsx';

class Layout extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="container main row">
                    <div className="col s12 white z-depth-1">
                        { this.props.children }
                    </div>
                </div>
            </div>
        );
    }
}

export default Layout;