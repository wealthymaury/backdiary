import React, { Component } from 'react';
import { Link } from "react-router-dom";


class Header extends Component {
    render() {
        return (
            <nav className="blue darken-3 nav-bottom">
                <a href="#contacts" className="js-list brand">REACTDIARY</a>
                <ul className="right">
                    <li><Link to="/contactos/crear"><i className="small mdi-content-add"></i></Link></li>
                    <li><Link to="/contactos"><i className="small mdi-action-subject"></i></Link></li>
                    <li><Link to="/contactos"><i className="small mdi-navigation-refresh"></i></Link></li>
                    <li><a href="/logout/"><i className="small mdi-navigation-close"></i></a></li>
                </ul>
            </nav> 
        );
    }
}

export default Header;