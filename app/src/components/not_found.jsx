import React, { Component } from 'react';

class NotFound extends Component {
    render() {
        return (
            <ul className="collection with-header border-none">
                <li className="collection-header">
                    <h5><center>Pagina no encontrada</center></h5>
                </li>
                <div className="content">
                    <br/>
                    <center>Lo sentimos mucho</center>
                </div>
            </ul>
        );
    }
}

export default NotFound;