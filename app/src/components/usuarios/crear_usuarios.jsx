import React, { Component } from 'react';
import { connect } from "react-redux";
import { Control, Form, actions } from 'react-redux-form';

import {addUser} from '../../actions/index';

class FormUsuarios extends Component {
    constructor(props) {
        super(props);

        this.firstNameRef = React.createRef();
        this.lastNameRef = React.createRef();
        this.userNameRef = React.createRef();
        this.emailRef = React.createRef();

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(user) {
        event.preventDefault();
        this.props.addUser(user);
    }

    render() {
        const erroresFirstName = (this.props.erroresUsuario.first_name.length > 0) ? <small className="error red-text">{this.props.erroresUsuario.first_name[0]}</small> : null;
        const erroresLastName = (this.props.erroresUsuario.last_name.length > 0) ? <small className="error red-text">{this.props.erroresUsuario.last_name[0]}</small> : null;
        const erroresUserName = (this.props.erroresUsuario.username.length > 0) ? <small className="error red-text">{this.props.erroresUsuario.username[0]}</small> : null;
        const erroresEmail = (this.props.erroresUsuario.email.length > 0) ? <small className="error red-text">{this.props.erroresUsuario.email[0]}</small> : null;
        
        return (
            <Form model="user" onSubmit={(user) => this.handleSubmit(user)} className="section paddingable" >
                <div className="row">
                    <div className="col s12">
                        <h5>Nuevo usuario</h5>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6">
                        <Control.text 
                            id="user.first_name" 
                            name="first_name" 
                            model="user.first_name"
                            className={"validate " + (this.props.erroresUsuario.first_name.length > 0 ? "invalid" : null)}
                            ref={this.firstNameRef} />
                        <label htmlFor="user.first_name">First name:</label>
                        {erroresFirstName}
                    </div>
                    <div className="input-field col s12 m6">
                        <Control.text 
                            id="user.last_name" 
                            name="last_name" 
                            model="user.last_name"
                            className={"validate " + (this.props.erroresUsuario.last_name.length > 0 ? "invalid" : null)}
                            ref={this.lastNameRef} />
                        <label htmlFor="user.last_name">Last name:</label>
                        {erroresLastName}
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6">
                        <Control.text 
                            id="user.username" 
                            name="username" 
                            model="user.username"
                            className={"validate " + (this.props.erroresUsuario.username.length > 0 ? "invalid" : null)}
                            ref={this.userNameRef} />
                        <label htmlFor="user.username">Username:</label>
                        {erroresUserName}
                    </div>
                    <div className="input-field col s12 m6">
                        <Control.text 
                            id="user.email" 
                            name="email" 
                            model="user.email"
                            className={"validate " + (this.props.erroresUsuario.email.length > 0 ? "invalid" : null)}
                            ref={this.emailRef} />
                        <label htmlFor="user.email">Email:</label>
                        {erroresEmail}
                    </div>
                </div>
                <div className="row">
                    <div className="col s12 right-align">
                        <button className="btn btn-primary js-submit">GUARDAR</button>
                    </div>
                </div>
            </Form>
        );
    }
}

// Mapea el state del store global a props
function mapStateToProps(state) {
    return {
        erroresUsuario: state.rootReducer.erroresUsuario
    };
}

// Mapea las acciones globales a props
function mapDispatchToProps(dispatch) {
    return {
        addUser: user => dispatch(addUser(user))
    };
}

// Creamos el componente que tiene conectadas las acciones de redux con react
// Para poder lanzarlas desde las props y tambien el state
export default connect(mapStateToProps, mapDispatchToProps)(FormUsuarios);
