import React, { Component } from 'react';

import DetalleUsuarioPerfil from './detalle_usuario_perfil.jsx';
import DetalleUsuarioAccount from './detalle_usuario_account.jsx';
import DetalleUsuarioTelefonos from './detalle_usuario_telefonos.jsx';

class DetalleUsuario extends Component {
    render() {
        return (
            <div className="paddingable">
                <div className="section">
                    <h5>Datos del usuario</h5>
                </div>

                <div className="divider"></div>

                <div className="section">
                    <div className="row">

                        <div className="col s12 m4 l3 center-align">
                            <DetalleUsuarioPerfil />
                        </div>

                        <div className="col s12 m8 l9">
                            <div className="row" id="account-region">
                                <DetalleUsuarioAccount />
                            </div>

                            <div className="row" id="phones-region">
                                <DetalleUsuarioTelefonos />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DetalleUsuario; 