import React, { Component } from 'react';

import DetalleUsuarioTelefono from './detalle_usuario_telefono.jsx';

class DetalleUsuarioTelefonos extends Component {
    render() {
        return (
            <div>
                <div className="col s12 section">
                    <div className="row valign-wrapper">
                        <div className="col s6 left-align valign">
                            <span className="grey-text">Telefonos</span>
                        </div>
                        <div className="col s6 right-align valign">
                            <i className="material-icons grey-text icon-mini js-add">add_circle</i>
                        </div>
                    </div>
                </div>
                <div className="col s12 section phones">
                    <DetalleUsuarioTelefono />
                    <DetalleUsuarioTelefono />
                    <DetalleUsuarioTelefono />
                </div>
            </div>
        );
    }
}

export default DetalleUsuarioTelefonos; 