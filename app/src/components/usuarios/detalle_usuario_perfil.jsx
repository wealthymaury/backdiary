import React, { Component } from 'react';

class DetalleUsuarioPerfil extends Component {
    render() {
        return (
            <div className="row">
                <div className="col s12 section">
                    <div className="row valign-wrapper">
                        <div className="col s6 left-align valign">
                            <span className="grey-text">Perfil</span>
                        </div>
                        <div className="col s6 right-align valign">
                            <i className="material-icons grey-text icon-mini js-edit">create</i>
                        </div>
                    </div>
                </div>
                <div className="col s12">
                    <img src="avatar" className="responsive circle" alt="" />
                </div>
                <div className="col s12">
                    <h5>job</h5>
                    <small><time>hace timestamp</time></small>
                </div>
            </div>
        );
    }
}

export default DetalleUsuarioPerfil; 