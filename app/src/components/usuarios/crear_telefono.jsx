import React, { Component } from 'react';

class FormTelefono extends Component {
    render() {
        return (
            <form className="section paddingable">
                <div className="row">
                    <div className="col s12">																
                        <h5 className="js-title"></h5>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12 m6">
                        <input id="contact-number" name="number" type="text" className="validate" />
                        <label htmlFor="contact-number">Numero:</label>
                    </div>
                    <div className="input-field col s12 m6">
                        <select id="contact-type" name="type" className="browser-default" defaultValue="">
                            <option value="" disabled>Selecciona...</option>
                            <option value="cel">Celular</option>
                            <option value="nex">Nextel</option>
                            <option value="tel">Fijo</option>
                        </select>
                    </div>
                </div>
                <div className="row">
                    <div className="col s12 right-align">								
                        <button className="btn btn-primary js-submit">GUARDAR</button>
                    </div>
                </div>
            </form>
        );
    }
}

export default FormTelefono; 