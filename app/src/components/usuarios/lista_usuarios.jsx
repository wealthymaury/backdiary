import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { getData } from "../../actions/index";

class ListaUsuarios extends Component {

    componentDidMount() {
        this.props.getData();
    }

    render() {
        return (
            <ul className="collection with-header border-none paddingable">
                <li className="collection-header">
                    <h5>Usuarios</h5>
                </li>
                <div className="content">
                    {
                        this.props.usuarios.map(el => (
                            <li className="collection-item avatar fix-padding-left" key={el.id}>
                                <img src={el.avatar} alt="username" className="circle" />
                                <span className="title">{el.username}</span>
                                <p>
                                    {el.first_name} <br />
                                    {el.last_name}
                                </p>

                                <div href="#!" className="secondary-content">
                                    <Link to={"/contactos/" + el.id + "/editar"}><i className="icon-mini mdi-editor-mode-edit small grey-text"></i></Link>
                                    <i className="icon-mini mdi-action-delete small grey-text"></i>
                                    <Link to={"/contactos/" + el.id}><i className="icon-mini mdi-content-send small grey-text"></i></Link>
                                </div>
                            </li>
                        ))
                    }
                </div>
            </ul>
        );
    }
}

function mapStateToProps(state) {
    return {
        usuarios: state.rootReducer.usuarios
    };
}

export default connect(mapStateToProps, { getData })(ListaUsuarios);