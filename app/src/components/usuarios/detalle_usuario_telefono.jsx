import React, { Component } from 'react';

class DetalleUsuarioPerfil extends Component {
    render() {
        return (
            <div className="Chip">
                <div className="Chip-phone">
                    <strong><span>number</span></strong>
                    <span>Tipos[type]</span>
                </div>
                <div className="Chip-options">
                    <i className="material-icons grey-text icon-mini js-edit">create</i>
                    <i className="material-icons grey-text icon-mini js-delete">delete</i>
                </div>
            </div>
        );
    }
}

export default DetalleUsuarioPerfil; 