import React, { Component } from 'react';

class DetalleUsuarioAccount extends Component {
    render() {
        return (
            <div>
                <div className="col s12 section">
                    <div className="row valign-wrapper margin-none">
                        <div className="col s6 left-align valign padding-none">
                            <span className="grey-text">Cuenta de usuario</span>
                        </div>
                        <div className="col s6 right-align valign padding-none">
                            <i className="material-icons grey-text icon-mini js-edit">create</i>
                        </div>
                    </div>
                </div>
                <div className="col s12 section">
                    <h4>username</h4>
                    <h5>first_name last_name</h5>
                    <h6>email</h6>
                </div>
            </div>
        );
    }
}

export default DetalleUsuarioAccount; 