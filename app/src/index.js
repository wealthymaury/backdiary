import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import store from "./store/index";
import Layout from "./components/Layout.jsx";
import ListaUsuarios from './components/usuarios/lista_usuarios.jsx';
import FormUsuarios from './components/usuarios/crear_usuarios.jsx';
import DetalleUsuario from './components/usuarios/detalle_usuario.jsx';
import FormTelefono from './components/usuarios/crear_telefono.jsx';
import NotFound from './components/not_found.jsx';

render(
    <Provider store={store}>
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route path="/contactos" exact name="contactos" component={ListaUsuarios} />
                    <Route path="/contactos/crear" exact name="crear-contactos" component={FormUsuarios} />
                    <Route path="/contactos/:id(\d+)" exact name="ver-contacto" component={DetalleUsuario} />
                    <Route path="/contactos/:id(\d+)/editar" exact name="editar-contacto" component={FormUsuarios} />
                    <Route path="/contactos/:id(\d+)/telefonos/registrar" exact name="registrar-telefono" component={FormTelefono} />
                    <Route component={NotFound} />
                </Switch>
            </Layout>
        </BrowserRouter>
    </Provider>,
    document.getElementById("app")
);