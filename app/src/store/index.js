/**
 * El store tiene todo el state de la app
 * El state lo genera un reducer
 */

import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { createForms } from 'react-redux-form';
import thunk from "redux-thunk";

import { forbiddenWordsMiddleware } from "../middleware";
import rootReducer from "../reducers/index";

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const initialUserState = {
    first_name: '',
    last_name: '',
    username: '',
    email: ''
};

const store = createStore(
    combineReducers({
        rootReducer,
        ...createForms({
            user: initialUserState,
        }),
    }),
    storeEnhancers(applyMiddleware(forbiddenWordsMiddleware, thunk))
);

export default store;