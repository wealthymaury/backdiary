/**
 * La accion solo es una actividad que puede pasar, payload es el cambio que se hara en el state
 * 
 */

import axios from 'axios';
import { actions } from 'react-redux-form';

import { ADD_USER, ADD_USER_ERROR, FOUND_BAD_WORD, USERS_LOADED } from "../constants/action-types";

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN"

export function addUser(payload) {
    return function(dispatch) {
        return axios.post("/api/users/", payload)
            .then(response => {
                dispatch({ type: ADD_USER, payload: response.data });
                return response;
            })
            .then((response) => {
                dispatch(actions.change('user.first_name', ''));
                dispatch(actions.change('user.last_name', ''));
                dispatch(actions.change('user.username', ''));
                dispatch(actions.change('user.email', ''));
                return response;
            })
            .catch(error => {
                dispatch({ type: ADD_USER_ERROR, payload: error.response.data });
            });
    };
}

export function foundBadWords() {
    return { type: FOUND_BAD_WORD };
}

export function getData() {
    return function(dispatch) {
        return axios.get("/api/users/")
            .then(response => {
                dispatch({ type: USERS_LOADED, payload: response.data });
            });
    };
}