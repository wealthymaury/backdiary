/**
 * El reducer es quien produce el state de la app
 * tan pronto como un store recive una señal de accion, 
 * llama al reducer el cual regresa el nuevo state dependiendo la accion que llega
 */

import { ADD_USER, ADD_USER_ERROR, FOUND_BAD_WORD, USERS_LOADED } from "../constants/action-types";

const initialState = {
    usuarios: [],
    erroresUsuario: {
        first_name: [],
        last_name: [],
        username: [],
        email: []
    }
};

function rootReducer(state = initialState, action) {
    // Usuario guardado de manera exitosa
    if (action.type === ADD_USER) {
        return {
            ...state,
            usuarios: state.usuarios.concat(action.payload),
            erroresUsuario: initialState.erroresUsuario
        };
    }

    // Error al guardar el usuario en el server
    if (action.type === ADD_USER_ERROR) {
        return {
            ...state,
            erroresUsuario: {
                ...state.erroresUsuario,
                ...action.payload
            }
        };
    }

    // Usuarios cargados correctamente
    if (action.type === USERS_LOADED) {
        return {
            ...state,
            usuarios: action.payload
        };
    }

    return state;
}

export default rootReducer;