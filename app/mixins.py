import hashlib
import requests # instalado con pip install requests

from django.conf import settings
from django.core.signing import TimestampSigner
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator

from rest_framework.decorators import detail_route
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from .models import Phone
from .serializers import PhoneSerializer

class LoginRequiredMixin(object):
	@method_decorator(login_required(login_url='/'))
	def dispatch(self, request, *args, **kwargs):
		return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)

class ListPhonesMixin(object):
	@detail_route(methods=['get'])
	def phones(self, request, pk=None):
		phones = Phone.objects.filter(user__pk = pk)
		serializer = PhoneSerializer(phones, many=True)
		return Response(serializer.data)

# esta clase es la que manda las cosas a tornado segun lo que ocurre
class UpdateHookMixin(object):
	def perform_create(self, serializer):
		super(UpdateHookMixin, self).perform_create(serializer)
		self._send_request(serializer.instance, 'POST')

	def perform_update(self, serializer):
		super(UpdateHookMixin, self).perform_update(serializer)
		self._send_request(serializer.instance, 'PUT')

	def perform_destroy(self, instance):
		self._send_request(instance, 'DELETE')
		super(UpdateHookMixin, self).perform_destroy(instance)

	def _send_request(self, obj, method):
		url = self._build_url(obj)

		if method in ('POST', 'PUT'):
			serializer = self.get_serializer(obj)
			renderer = JSONRenderer()
			context = {'request': self.request}
			body = renderer.render(serializer.data, renderer_context=context)
		else:
			body = None

		headers = {
			'context-type': 'application/json',
			'X-Signature': self._build_signarute(method, url, body) # devuelve una cadena encriptada con {metodo}:{url}:{body}
		}

		try:
			response = requests.request(method, url, data=body, timeout=0.5, headers=headers)
			response.raise_for_status()
		except requests.exceptions.ConnectionError:
			pass
		except requests.exceptions.Timeout:
			pass
		except requests.exceptions.RequestException, e:
			pass

	def _build_url(self, obj):
		if isinstance(obj, User):
			model = 'user'
		else:
			model = obj.__class__.__name__.lower()
		
		return '{protocol}://{server}/{model_name}/{pk}'.format(
			protocol = 'https' if settings.SOCKET_SECURE else 'http',
			server = settings.SOCKET_SERVER,
			model_name = model,
			pk = obj.pk
		)

	def _build_signarute(self, method, url, body):
		signer = TimestampSigner(settings.SOCKET_SECRET)
		value = '{method}:{url}:{body}'.format(
			method=method.lower(),
			url=url,
			body=hashlib.sha256(body or b'').hexdigest()
		)
		return signer.sign(value)


