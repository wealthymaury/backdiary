from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import UserProfile, Phone



@admin.register(Phone)
class PhoneAdmin(admin.ModelAdmin):
	list_display = ('number', 'user', 'type', 'timestamp')
	list_filter = ('type',)
	search_fields = ('number', 'type')

# para que el perfil de ususario aparezca junto con el usuario
class UserProfileInline(admin.StackedInline):
	model = UserProfile
	can_delete = False
	verbose_name_plural = 'profile'
	filter_horizontal = ('phones',)

admin.site.unregister(User)
@admin.register(User)
class UserAdmin(UserAdmin):
	inlines = (UserProfileInline, )
	list_display = ('show_avatar', 'username', 'email', 'first_name', 'last_name', 'is_staff')

	def show_avatar(self, obj):
		if obj.userprofile.get_thumb_avatar() is None:
			return "Avatar not found"

		tag = '<img src="%s" title="%s"/>' % (obj.userprofile.get_thumb_avatar(), obj)
		return tag

	show_avatar.allow_tags = True
	show_avatar.admin_order_field = 'name'