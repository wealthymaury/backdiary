from django.contrib.auth.forms import AuthenticationForm
from django import forms 

class MyAuthenticationForm(AuthenticationForm):
	attrs = {
		'autocomplete' : "off",
		'class' : 'validate'
	}
	username = forms.CharField(max_length=254, widget=forms.TextInput(attrs=attrs))
	password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs=attrs))