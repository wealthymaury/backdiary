from django.contrib.auth.models import User

from rest_framework import viewsets

from .serializers import UserSerializer, UserProfileSerializer, PhoneSerializer
from .models import UserProfile, Phone
from .mixins import ListPhonesMixin, UpdateHookMixin

class UserViewSet(UpdateHookMixin, ListPhonesMixin, viewsets.ModelViewSet):
	queryset = User.objects.all().order_by('-id')
	serializer_class = UserSerializer

class UserProfileViewSet(UpdateHookMixin, ListPhonesMixin, viewsets.ModelViewSet):
	queryset = UserProfile.objects.all()
	serializer_class = UserProfileSerializer

class PhoneViewSet(UpdateHookMixin, viewsets.ModelViewSet):
	queryset = Phone.objects.all()
	serializer_class = PhoneSerializer