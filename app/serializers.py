from django.contrib.auth.models import User

from rest_framework import serializers

from .models import UserProfile, Phone
from .fields import Base64ImageField

class SimpleUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ("id", "username", "first_name", "last_name", "email")
		read_only_fields = ('username',)

class UserSerializer(serializers.ModelSerializer):
	avatar = serializers.SerializerMethodField(read_only=True)

	class Meta:
		model = User
		fields = ("id", "avatar", "username", "first_name", "last_name", "email")
		# read_only_fields = ('username',)

	def get_avatar(self, obj):
		return obj.userprofile.get_thumb_avatar()

class PhoneSerializer(serializers.ModelSerializer):
	class Meta:
		model = Phone

class UserProfileSerializer(serializers.ModelSerializer):
	user = SimpleUserSerializer(read_only=True)
	# phones = PhoneSerializer(many=True)
	timestamp = serializers.SerializerMethodField(read_only=True)
	avatar = Base64ImageField(max_length=None, allow_empty_file=True, use_url=True,)
	
	class Meta:
		model = UserProfile
		depth = 1
		exclude = ('phones',)

	def get_timestamp(self, obj):
		return obj.date()
