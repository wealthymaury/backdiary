from django.conf import settings
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.core.signing import TimestampSigner
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import FormView, TemplateView

from .forms import MyAuthenticationForm
from .mixins import LoginRequiredMixin

class LoginFormView(FormView):
	form_class = MyAuthenticationForm
	template_name = "home/index.html"
	success_url = "/contactos/"

	def get(self, request, *args, **kwargs):
		if self.request.user.is_authenticated():
			return HttpResponseRedirect(self.get_success_url())

		return super(LoginFormView, self).get(request, *args, **kwargs)

	def form_valid(self, form):
		login(self.request, form.get_user())

		return super(LoginFormView, self).form_valid(form)

	def form_invalid(self, form):
		for field in form:
			if field.errors:
				form.fields[field.name].widget.attrs['class'] = "validate invalid"

		return super(LoginFormView, self).form_invalid(form)


class AppTemplateView(LoginRequiredMixin, TemplateView):
	template_name = 'app/main_react.html'

	def get_context_data(self, **kwargs):
		context = super(AppTemplateView, self).get_context_data(**kwargs)
		signer = TimestampSigner(settings.SOCKET_SECRET)
		channel = signer.sign(1)
		url = '{protocol}://{server}/socket?channel={channel}'.format(
			protocol = 'wss' if settings.SOCKET_SECURE else 'ws',
			server = settings.SOCKET_SERVER,
			channel = channel
		)
		context.update(socket=url)
		return context
	
