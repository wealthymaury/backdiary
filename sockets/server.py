import os
import json
import logging
import signal
import time

from collections import defaultdict

from django.conf import settings
from django.core.signing import TimestampSigner, BadSignature, SignatureExpired

from redis import Redis
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import define, parse_command_line, options
from tornado.web import Application
from tornadoredis import Client

from subscribers import RedisSubscriber
from handlers import SubscriberHandler, UpdatesHandler

define('debug', default=True, type=bool, help='Run in debug mode')
define('port', default=os.environ.get("PORT", 3000), type=int, help='Server port')
define('allowed_hosts', multiple=True, default='localhost:8000,', help='Allowed hosts for cross domain connectinos')

class BackbiarySocketsApplication(Application):

	def __init__(self, **kwargs):
		routes = [
			(r'/socket', SubscriberHandler),
			(r'/(?P<model_name>user|phone|userprofile)/(?P<pk>[0-9]+)', UpdatesHandler)
		]
		super(BackbiarySocketsApplication, self).__init__(routes, **kwargs)
		self.subscriber = RedisSubscriber(Client())
		self.publisher = Redis()
		self._key = 'tumameama' # es la misma que en django
		self.signer = TimestampSigner(self._key)

	def add_suscriber(self, channel, subscriber):
		logging.info('Adding '+ subscriber.uid +' to channel ' + channel)
		self.subscriber.subscribe(['all', channel], subscriber)

	def remove_subscriber(self, channel, subscriber):
		logging.info('Removing '+ subscriber.uid +' to channel ' + channel)
		self.subscriber.unsubscribe(channel, subscriber)
		self.subscriber.unsubscribe('all', subscriber)

	def broadcast(self, message, channel=None, sender=None):
		channel = 'all' if channel is None else channel
		message = json.dumps({ # armando un JSON
			'sender': sender and sender.uid,
			'message': message
		})
		logging.info('Sending data to channel ' + channel)
		self.publisher.publish(channel, message)

# solo es para avizar cuando paramos el servidor
def shutdown(server):
	ioloop = IOLoop.instance()
	logging.info('Stoping server.')
	server.stop()

	def finalize():
		ioloop.stop()
		logging.info('Stoped.')

	ioloop.add_timeout(time.time() + 0.2, finalize)

if __name__ == '__main__':
	parse_command_line()
	application = BackbiarySocketsApplication(debug=options.debug)
	server = HTTPServer(application)
	server.listen(options.port)
	signal.signal(signal.SIGINT, lambda sig, frame: shutdown(server))
	logging.info('Starting server on port:{}'.format(options.port))
	IOLoop.instance().start()



