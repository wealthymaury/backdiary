import json

from tornadoredis.pubsub import BaseSubscriber

# recib un mensaje en JSON y lo conviete a dict y le escribe el mensaje a todos los del canal
class RedisSubscriber(BaseSubscriber):
	def on_message(self, msg):
		if msg and msg.kind == 'message':
			try:
				message = json.loads(msg.body) # de json a disct
				sender = message['sender']
				message = message['message']
			except (ValueError, KeyError):
				message = msg.body
				sender = None
			subscribers = list(self.subscribers[msg.channel].keys())
			for subscriber in subscribers:
				if sender is None or sender != subscriber.uid:
					try:
						subscriber.write_message(message)
					except tornado.websocket.WebSocketClosedError:
						self.unsubscribe(msg.channel, subscriber)
		super(RedisSubscriber, self).on_message(msg)